package com.jontech.jglfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/***
 * This class manages the creation of a new question into the DB*
 *
 */
public class CreateActivity extends AppCompatActivity {

    //Database varibales
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;


    //Variable declaration
    private TextView questionView, answerTxt1, answerTxt2, answerTxt3, answerTxt4, infoStatus, longitudeTxt, latitudeTxt;
    private Button showBtn, sendBtn;
    private List<RadioButton> radioBtnList = new ArrayList<>();

    //Entities
    private QuestionEntity questionEntity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        initializeDB();
        addBtnListeners();

    }


    //Load button listeners
    public void addBtnListeners() {

        sendBtn = (Button)findViewById(R.id.addQuestionBtn);
        showBtn = (Button)findViewById(R.id.listQBtn);

        infoStatus = (TextView)findViewById(R.id.infoStatusTxt);

        //These are references to the TextView from the UI
        questionView = (TextView)findViewById(R.id.questionTxt);
        answerTxt1 = (TextView)findViewById(R.id.answerTxt1);
        answerTxt2 = (TextView)findViewById(R.id.answerTxt2);
        answerTxt3 = (TextView)findViewById(R.id.answerTxt3);
        answerTxt4 = (TextView)findViewById(R.id.answerTxt4);

        //Those are loaded into a List to later check which is the correct answer when
        //creating a new question
        radioBtnList.add((RadioButton)findViewById(R.id.radioButton));
        radioBtnList.add((RadioButton)findViewById(R.id.radioButton2));
        radioBtnList.add((RadioButton)findViewById(R.id.radioButton3));
        radioBtnList.add((RadioButton)findViewById(R.id.radioButton4));

        latitudeTxt = (TextView)findViewById(R.id.latitudeTxt);
        longitudeTxt = (TextView)findViewById(R.id.longitudeTxt);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check if any of the user input views are empty (they should not ne empty)
                if (questionView.getText().toString().isEmpty()) {
                    infoStatus.setText("You have not provided a valid question");
                    return;
                } else if (answerTxt1.getText().toString().isEmpty()) {
                    infoStatus.setText("Answer1 cannot be empty!");
                    return;
                } else if (answerTxt2.getText().toString().isEmpty()) {
                    infoStatus.setText("Answer2 cannot be empty!");
                    return;
                } else if (answerTxt3.getText().toString().isEmpty()) {
                    infoStatus.setText("Answer3 cannot be emtpy!");
                    return;
                } else if (answerTxt4.getText().toString().isEmpty()) {
                    infoStatus.setText("Answer4 cannot be emtpy!");
                    return;
                }

                //Check if the question is valid (can't be only numeres) -> Doubt this for now

                //Check if there is no validAnswer checked (radio boxes)

                int validAns = -1;

                for (int i = 0; i < radioBtnList.size(); i++) {
                    System.out.println("[DEBUG] Checking valid answer is selected");
                    if (radioBtnList.get(i).isChecked()) {
                        validAns = i+1;
                    } else if (validAns == -1 && i == radioBtnList.size()) {
                        infoStatus.setText("You must check a valid answer!");
                        return;
                    }
                }

                //Question creation
                questionEntity = new QuestionEntity(questionView.getText().toString()
                                                    ,answerTxt1.getText().toString()
                                                    ,answerTxt2.getText().toString()
                                                    ,answerTxt3.getText().toString()
                                                    ,answerTxt4.getText().toString()
                                                    ,validAns
                                                    ,Double.parseDouble(latitudeTxt.getText().toString())
                                                    ,Double.parseDouble(longitudeTxt.getText().toString())
                );

                try {
                    databaseReference.push().setValue(questionEntity);
                    infoStatus.setText("Question loaded successfully into the DB");
                } catch (Exception e) {
                    infoStatus.setText("Question could not be uploaded to the DB");
                    System.out.println("[ERROR] " + e);
                }
            }
        });

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Send to a recyclerView object to look at all the game questions
            }
        });



    }


    private void initializeDB() {
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference().child("questions");
    }




}
