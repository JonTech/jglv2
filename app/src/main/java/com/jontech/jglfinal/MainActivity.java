package com.jontech.jglfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {


    private Button playbtn;
    private Button exitBtn;

    private FirebaseDatabase database;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeDB();
        addButtonListeners();


    }



    private void addButtonListeners() {

        playbtn = (Button)findViewById(R.id.loginBtn);
        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent that goes straight to the gameplay menu but initialize the DB as well
                Intent i = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(i);
            }
        });


        exitBtn = (Button)findViewById(R.id.returnBtnMenu);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0); //Out of this shit program
            }
        });

    }

    private void initializeDB() {
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference().child("questions");
    }

}
