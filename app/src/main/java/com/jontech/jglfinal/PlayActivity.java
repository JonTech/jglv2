package com.jontech.jglfinal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PlayActivity extends AppCompatActivity implements OnMapReadyCallback {

    //DB variables
    private FirebaseDatabase database;
    private DatabaseReference questionsReference;
    private DatabaseReference databaInfoReference; //Info about the number of children it has

    private int numExistingQuestions = 1; //DB works with index 1 as first...
    private int currentQuestionID = 1;
    private int solutionIndex = 0;


    //UI variables
    private TextView questionViewTxt;
    private List<RadioButton> radioButtonList = new ArrayList<>();
    private Button solveBtn;

    //This will hold the google map in place
    private SupportMapFragment supportMapFragment;

    //Map variables
    private GoogleMap googleMap;
    private double latitude = 41.390; //Initial default latitude
    private double longitude = 2.150; //Initial default longitude


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        addBtnListeners();
        initiateMap(); //Instantiate the map
        //TODO: Do not activate till DB is ready to bring data
        loadQuestionsFromRTDB();
        loadRandomQuestion();


    }

    private void addBtnListeners() {

        questionViewTxt = (TextView)findViewById(R.id.questionViewTxt);
        radioButtonList.add((RadioButton)findViewById(R.id.optTadioBtn1));
        radioButtonList.add((RadioButton)findViewById(R.id.optTadioBtn2));
        radioButtonList.add((RadioButton)findViewById(R.id.optTadioBtn3));
        radioButtonList.add((RadioButton)findViewById(R.id.optTadioBtn4));


        solveBtn = (Button)findViewById(R.id.solveBtn);
        solveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButtonList.get(solutionIndex - 1).isChecked()) {
                    Toast.makeText(getApplicationContext(), "Respues correcta!", Toast.LENGTH_SHORT).show();
                    currentQuestionID++;
                    loadRandomQuestion();
                } else {
                    Toast.makeText(getApplicationContext(), "Respuesta Incorrecta", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private void loadQuestionsFromRTDB() {

        database = FirebaseDatabase.getInstance();
        questionsReference = database.getReference().child("questions");
        databaInfoReference = database.getReference().child("questions").getParent();
        databaInfoReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                System.out.println("Updated existing number of questions to: " + numExistingQuestions);
                numExistingQuestions = (int)dataSnapshot.getChildrenCount();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    private void loadRandomQuestion() {
        System.out.println("[DEBUG] Current question: " + currentQuestionID + " max: " + numExistingQuestions);
        //If the question max number is reached, reset
        if (currentQuestionID > numExistingQuestions) {
            System.out.println("[DEBUG] Resetting current question to 1");
            this.currentQuestionID = 1; //Because the index is at 1
        }

        //Question setup
        questionsReference.orderByChild("messages")
                .limitToFirst(currentQuestionID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //If there are questions to load (that is some debug shit right there)
                        if (dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot dataSnap : dataSnapshot.getChildren()) {
                                String question = dataSnap.child("question").getValue().toString();
                                String answer1 = dataSnap.child("answer1").getValue().toString();
                                String answer2 = dataSnap.child("answer2").getValue().toString();
                                String answer3 = dataSnap.child("answer3").getValue().toString();
                                String answer4 = dataSnap.child("answer4").getValue().toString();

                                solutionIndex = Integer.parseInt(dataSnap.child("validAnswer").getValue().toString());

                                //TODO: Doule or double?
                                latitude = Double.parseDouble(dataSnap.child("latitude").getValue().toString());
                                longitude = Double.parseDouble(dataSnap.child("longitude").getValue().toString());


                                //Load data into the UI
                                questionViewTxt.setText(question);
                                radioButtonList.get(0).setText(answer1);
                                radioButtonList.get(1).setText(answer2);
                                radioButtonList.get(2).setText(answer3);
                                radioButtonList.get(3).setText(answer4);

                                //TODO: Set up the map to locate country here

                                setupMap(latitude, longitude);

                                System.out.println("[DEBUG-DATA] \n" +
                                        "question: " + question + "\n" +
                                        "answer1: " + answer1 + "\n" +
                                        "answer2: " + answer2 + "\n" +
                                        "answer3: " + answer3 + "\n" +
                                        "answer4: " + answer4 + "\n" +
                                        "latitude: " + latitude + "\n" +
                                        "longitude: " + longitude);

                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
    }

    /***
     * Loads the fragment which supports the google map
     *
     */
    private void initiateMap() {
        this.supportMapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        this.supportMapFragment.getMapAsync(this);
    }

    /***
     * An event which listens for a call to build the Map, (loads the default latitude and longitude) -> DEBUG PURPOSE
     *
     * @param googleMaparg
     */
    @Override
    public void onMapReady(GoogleMap googleMaparg) {
        this.googleMap = googleMaparg;
        setupMap(this.latitude, this.longitude); //Setup map with default shit
    }

    /***
     * This handlers will change the latitude and longitude of such map
     *
     *
     * @param latitude
     * @param longitude
     */
    private void setupMap(double latitude, double longitude) {

        //Instantiate with default location (user's current location)
        this.googleMap.clear();
        LatLng marker = new LatLng(latitude, longitude);
        this.googleMap.addMarker(new MarkerOptions().position(marker)); //Set up a marker
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(marker)); //Move camera to position
        this.googleMap.setMinZoomPreference(3);
        this.googleMap.getUiSettings().setAllGesturesEnabled(false);
        this.googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));

    }
}
