package com.jontech.jglfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {

    private Button playBtn;
    private Button returnBtn;
    private Button createaBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        addButtonListeners();

    }

        //BRB to continue coding :)


    private void addButtonListeners() {

        playBtn = (Button)findViewById(R.id.playBtn);
        returnBtn = (Button)findViewById(R.id.returnBtnMenu);
        createaBtn = (Button)findViewById(R.id.createBtn);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Play intent here
                Intent i = new Intent(MenuActivity.this, PlayActivity.class);
                startActivity(i);
            }
        });

        returnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Add toast?
                System.exit(0);//Will end the current activity and "return" to the previous which is the Game launcher
            }
        });

        createaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuActivity.this, CreateActivity.class);
                startActivity(i);
            }
        });


    }


}
