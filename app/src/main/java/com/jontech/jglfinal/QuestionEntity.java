package com.jontech.jglfinal;

public class QuestionEntity {

    private String question;
    private String answer1, answer2, answer3, answer4;
    private int validAnswer;
    private double latitude, longitude;

    public QuestionEntity(String question, String answer1, String answer2, String answer3, String answer4, int validAnswer, double latitude, double longitude) {
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.validAnswer = validAnswer;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public int getValidAnswer() {
        return validAnswer;
    }

    public void setValidAnswer(int validAnswer) {
        this.validAnswer = validAnswer;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "QuestionEntity{" +
                "question='" + question + '\'' +
                ", answer1='" + answer1 + '\'' +
                ", answer2='" + answer2 + '\'' +
                ", answer3='" + answer3 + '\'' +
                ", answer4='" + answer4 + '\'' +
                ", validAnswer=" + validAnswer +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
